﻿using PasswordGenerator.Model.Configuration;
using PasswordGenerator.Model.I18n;
using PasswordGenerator.Utilities.I18n;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace PasswordGenerator.Views
{
    public partial class DraggablePopupWindow : Popup
    {
        // Local identificators to note which changes should be applied upon APPLY button click.
        private bool useNeaningfulWords = false;
        private bool changeLanguage = false;
        private bool setCustomLength = false;
        private bool saveAddedItems = false;

        // Local state holder fields
        private int customLength = 0;
        //TODO:
        private Language currentPageLanguage = AppContext.GetInstance().CurrentLanguage;
        private ISet<int> numberAppends = new HashSet<int>();
        private ISet<char> letterAppends = new HashSet<char>();
        private ISet<char> specialAppends = new HashSet<char>();
        private ISet<string> wordAppends = new HashSet<string>();

        public DraggablePopupWindow()
        {
            InitializeComponent();
            MakeDraggable();
        }

        private void popCustomize_Opened(object sender, System.EventArgs e)
        {
            cbLanguage.ItemsSource = AppContext.GetInstance().GetAvailableLanguages();
            cbCatalog.ItemsSource = AppContext.GetInstance().GetAvailableCatalogs();

            Language language = AppContext.GetInstance().CurrentLanguage;
            Translator.PollForTranslate(this, language);
        }

        private void cbLanguage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Language language = AppContext.DEFAULT_LANGUAGE;

            ComboBox cbLanguageList = (sender as ComboBox);
            string langName = (cbLanguageList.SelectedItem as string);
            if (string.Equals(langName, AppContext.LANG_EN_US))
            {
                language = new EnglishUS();
            }
            else if (string.Equals(langName, AppContext.LANG_DE))
            {
                language = new Deutsch();
            }
            else
            {
                language = new Bulgarian();
            }

            // Notify the change
            currentPageLanguage = language;
            changeLanguage = true;
        }

        private void cbCatalog_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCatalog.SelectedItem == null)
            {
                return;
            }

            if (cbCatalog.SelectedItem.Equals(currentPageLanguage.ContentLettersCheckBox))
            {
                lbResources.ItemsSource = AppContext.GetInstance().Letters;
            }
            else if (cbCatalog.SelectedItem.Equals(currentPageLanguage.ContentNumbersCheckBox))
            {
                lbResources.ItemsSource = AppContext.GetInstance().Numbers;
            }
            else if (cbCatalog.SelectedItem.Equals(currentPageLanguage.ContentSpecialsCheckBox))
            {
                lbResources.ItemsSource = AppContext.GetInstance().Specials;
            }
            else
            {
                lbResources.ItemsSource = AppContext.GetInstance().Words;
            }

            lbResources.Items.Refresh();
        }

        private void chkWords_Checked(object sender, RoutedEventArgs e)
        {
            // Notify the change
            useNeaningfulWords = true;
        }

        private void chkWords_Unchecked(object sender, RoutedEventArgs e)
        {
            // Notify the change
            useNeaningfulWords = false;
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            // Apply all RELEVANT changes
            AppContext.GetInstance().UseMeaningfulWords = useNeaningfulWords;

            if (setCustomLength && (customLength > 0))
            {
                AppContext.GetInstance().CustomPWLength = customLength;
            }

            if (changeLanguage)
            {
                AppContext.GetInstance().CurrentLanguage = currentPageLanguage;
                AppContext.GetInstance().SaveCurrentLanguage();
                Translator.PollForTranslate(this, currentPageLanguage);
            }

            if (saveAddedItems)
            {
                AppContext.GetInstance().SaveCatalogAppends(letterAppends, specialAppends, numberAppends, wordAppends);
                lbResources.Items.Refresh();
            }

        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show(currentPageLanguage.ErrMsgConfirmResetMessageBox, currentPageLanguage.ErrConfirmCaptionMessageBox, MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                // Hardcoded default settings
                chkWords.IsChecked = false;
                useNeaningfulWords = false;
                setCustomLength = true;
                changeLanguage = true;

                AppContext.GetInstance().CustomPWLength = 0;

                currentPageLanguage = AppContext.DEFAULT_LANGUAGE;
                Translator.PollForTranslate(this, currentPageLanguage);
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            IsOpen = false;
        }

        private void btnLength_Click(object sender, RoutedEventArgs e)
        {
            int inputLength = 0;
            int.TryParse(Microsoft.VisualBasic.Interaction.InputBox(currentPageLanguage.ContentNewLengthInputBox, currentPageLanguage.CaptionInputBox,
                string.Empty, (int)Width / 2, (int)Height / 2), out inputLength);

            if (inputLength <= 0)
            {
                MessageBox.Show(currentPageLanguage.ErrMsgInvalidInputMessageBox, currentPageLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            customLength = inputLength;
            setCustomLength = true;
        }

        private void btnLetter_Click(object sender, RoutedEventArgs e)
        {
            char inputLetter = '\0';
            char.TryParse(Microsoft.VisualBasic.Interaction.InputBox(currentPageLanguage.ContentNewLetterInputBox, currentPageLanguage.CaptionInputBox,
                string.Empty, (int)Width / 2, (int)Height / 2), out inputLetter);

            if (!char.IsLetter(inputLetter))
            {
                MessageBox.Show(currentPageLanguage.ErrMsgInvalidInputMessageBox, currentPageLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            saveAddedItems = true;
            letterAppends.Add(inputLetter);
        }

        private void btnNumber_Click(object sender, RoutedEventArgs e)
        {
            int inputNumber = 0;
            int.TryParse(Microsoft.VisualBasic.Interaction.InputBox(currentPageLanguage.ContentNewNumberInputBox, currentPageLanguage.CaptionInputBox,
              string.Empty, (int)Width / 2, (int)Height / 2), out inputNumber);

            if (!char.IsDigit((char)inputNumber))
            {
                MessageBox.Show(currentPageLanguage.ErrMsgInvalidInputMessageBox, currentPageLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            saveAddedItems = true;
            numberAppends.Add(inputNumber);
        }

        private void btnSpecial_Click(object sender, RoutedEventArgs e)
        {
            char inputSpecial = '\0';
            char.TryParse(Microsoft.VisualBasic.Interaction.InputBox(currentPageLanguage.ContentNewSpecialInputBox, currentPageLanguage.CaptionInputBox,
                string.Empty, (int)Width / 2, (int)Height / 2), out inputSpecial);

            if (!char.IsSymbol(inputSpecial))
            {
                MessageBox.Show(currentPageLanguage.ErrMsgInvalidInputMessageBox, currentPageLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            saveAddedItems = true;
            specialAppends.Add(inputSpecial);
        }

        private void btnWord_Click(object sender, RoutedEventArgs e)
        {
            string inputWord = Microsoft.VisualBasic.Interaction.InputBox(currentPageLanguage.ContentNewWordInputBox, currentPageLanguage.CaptionInputBox,
                string.Empty, (int)Width / 2, (int)Height / 2);

            if (!inputWord.All(char.IsLetter))
            {
                MessageBox.Show(currentPageLanguage.ErrMsgInvalidInputMessageBox, currentPageLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            saveAddedItems = true;
            wordAppends.Add(inputWord);
        }

        private void MakeDraggable()
        {
            var thumb = new Thumb
            {
                Width = 0,
                Height = 0,
            };

            contentCanvas.Children.Add(thumb);

            MouseDown += (sender, e) =>
            {
                thumb.RaiseEvent(e);
            };

            thumb.DragDelta += (sender, e) =>
            {
                HorizontalOffset += e.HorizontalChange;
                VerticalOffset += e.VerticalChange;
            };
        }
    }
}
