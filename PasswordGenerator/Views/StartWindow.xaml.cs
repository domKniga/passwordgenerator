﻿using PasswordGenerator.Model.Configuration;
using PasswordGenerator.Utilities.I18n;
using PasswordGenerator.Utilities.UI;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace PasswordGenerator.Views
{
    public partial class StartWindow : Page
    {
        public StartWindow()
        {
            InitializeComponent();
        }

        private const string MAIN_WINDOW = "/Views/MainWindow.xaml";

        private void lblStart_MouseDoubleClick(object sender, RoutedEventArgs e)
        {
            NavigationService navigator = NavigationService.GetNavigationService(this);
            navigator.Navigate(new Uri(MAIN_WINDOW, UriKind.RelativeOrAbsolute));
        }

        private void lblStart_MouseEnter(object sender, RoutedEventArgs e)
        {
            UIService.HighlightLabel(sender as Label);
        }

        private void lblStart_MouseLeave(object sender, RoutedEventArgs e)
        {
            UIService.UnhighlightLabel(sender as Label);
        }

        private void startPage_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Translator.PollForTranslate(this, AppContext.GetInstance().CurrentLanguage);
        }
    }
}
