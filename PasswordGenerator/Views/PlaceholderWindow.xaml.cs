﻿using PasswordGenerator.Model.Configuration;
using System;
using System.Windows;
using System.Windows.Navigation;

namespace PasswordGenerator.Views
{
    public partial class PlaceholderWindow : NavigationWindow
    {
        public PlaceholderWindow()
        {
            InitializeComponent();
        }

        private void placeholder_Initialized(object sender, System.EventArgs e)
        {
            try
            {
                AppContext.GetInstance().LoadCatalogs();
            }
            catch (Exception)
            {
                MessageBox.Show("Loading password letters catalog failed!", "ERROR", MessageBoxButton.OK);
                Environment.Exit(1);
            }
        }
    }
}
