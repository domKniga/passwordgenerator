﻿using PasswordGenerator.Model.Configuration;
using PasswordGenerator.Model.I18n;
using PasswordGenerator.Utilities.Data;
using PasswordGenerator.Utilities.I18n;
using PasswordGenerator.Utilities.Locators;
using PasswordGenerator.Utilities.UI;
using System;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PasswordGenerator.Views
{
    public partial class MainWindow : Page
    {
        private string password = string.Empty;
        private bool isPasswordVisible = false;

        private string[] allPwSetFilePaths = null;
        private string selectedPwSetFilePath = string.Empty;

        private Language currentLanguage = new EnglishUS();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void option_Checked(object sender, RoutedEventArgs e)
        {
            var selectorFromGroupBox = (sender as ToggleButton);
            if (selectorFromGroupBox is RadioButton)
            {
                UIService.UnsignalAlert(lblStatus, grpLength, selectorFromGroupBox);
            }
            else
            {
                UIService.UnsignalAlert(lblStatus, grpSymbols, selectorFromGroupBox);
            }
        }

        private void option_Unchecked(object sender, RoutedEventArgs e)
        {
            var selectorFromGroupBox = (sender as ToggleButton);
            var groupBox = ((selectorFromGroupBox.Parent as StackPanel).Parent as GroupBox);

            Brush stateBrush = (Equals(groupBox.BorderBrush, UIService.BRUSH_ERROR) || !AppContext.GetInstance().UseMeaningfulWords)
                ? UIService.BRUSH_DEFAULT
                : UIService.BRUSH_SELECTED;
            UIService.ResetGroupBoxUIState(groupBox, selectorFromGroupBox, stateBrush);
        }

        private void lbPWSets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var lbPWSets = (sender as ListBox);
            var selectedPwSetContainer = (lbPWSets.SelectedItem as StackPanel);
            var tblkPWSetName = (selectedPwSetContainer.Children[1] as TextBlock);

            foreach (var pwSetFilePath in allPwSetFilePaths)
            {
                if (!string.IsNullOrEmpty(pwSetFilePath) && pwSetFilePath.EndsWith(tblkPWSetName.Text + ".txt"))
                {
                    selectedPwSetFilePath = pwSetFilePath;
                }
            }

            lbPWSets.Items.Refresh();
        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            UIElementCollection radioButtons = paneRadioButtons.Children;
            UIElementCollection checkBoxes = paneCheckBoxes.Children;

            if (!PWValidator.IsLengthSelected(radioButtons) && !PWValidator.AreSymbolsIncluded(checkBoxes))
            {
                UIService.SignalAlert(lblStatus, currentLanguage.ContentChoicesMissingStatusLabel, grpLength, grpSymbols);
                return;
            }
            else if (!PWValidator.IsLengthSelected(radioButtons))
            {
                UIService.SignalAlert(lblStatus, currentLanguage.ContentLengthMissingStatusLabel, grpLength);
                return;
            }
            else if (!PWValidator.AreSymbolsIncluded(checkBoxes))
            {
                UIService.SignalAlert(lblStatus, currentLanguage.ContentSymbolsMissingStatusLabel, grpSymbols);
                return;
            }

            LoadGeneratedPassword();

            if (!PWValidator.ValidatePasswordLength(password))
            {
                return;
            }

            UIService.HidePassword(txtPassBox, password);
            PWValidator.DisplayPasswordStrength(lblStrength, password);
        }

        private void LoadGeneratedPassword()
        {
            bool lettersOn = chkLetters.IsChecked ?? false;
            bool numbersOn = chkNumbers.IsChecked ?? false;
            bool specialsOn = chkSpecials.IsChecked ?? false;
            bool wordsOn = AppContext.GetInstance().UseMeaningfulWords;

            bool lengthFour = radFour.IsChecked ?? false;
            bool lengthEight = radEight.IsChecked ?? false;
            bool lengthTwelve = radTwelve.IsChecked ?? false;

            int length = DetermineLength(lengthFour, lengthEight, lengthTwelve);

            password = PWGenerator.GeneratePassword(length, lettersOn, numbersOn, specialsOn, wordsOn);
        }

        private int DetermineLength(bool fourSelected, bool eightSelected, bool twelveSelected)
        {
            if (AppContext.GetInstance().CustomPWLength > 0)
            {
                return AppContext.GetInstance().CustomPWLength;
            }
            else if (fourSelected)
            {
                return (int)Length.Minimum;
            }
            else if (eightSelected)
            {
                return (int)Length.Average;
            }
            else
            {
                return (int)Length.Maximum;
            }
        }

        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(password))
            {
                UIService.ShowStatus(lblStatus, UIService.BRUSH_ERROR, currentLanguage.ContentNoPasswordToCopyStatusLabel);
                return;
            }

            Clipboard.SetText(password);
            UIService.ShowStatus(lblStatus, UIService.BRUSH_VALID, currentLanguage.ContentCopiedToClipboardStatusLabel);
            UIService.ToggleButtonImage(btnCopy, ResourceLocator.PATH_ICON_COPY_ACTIVE);
        }

        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(password))
            {
                UIService.ShowStatus(lblStatus, UIService.BRUSH_ERROR, currentLanguage.ContentNoPasswordToShowStatusLabel);
                return;
            }

            // Toggle visibility implementation...
            isPasswordVisible = !isPasswordVisible;
            if (isPasswordVisible)
            {
                UIService.ShowPassword(txtPassBox, password);
                UIService.ToggleButtonImage(btnShow, ResourceLocator.PATH_ICON_SHOW_ACTIVE);
            }
            else
            {
                UIService.HidePassword(txtPassBox, password);
                UIService.ToggleButtonImage(btnShow, ResourceLocator.PATH_ICON_SHOW_INACTIVE);
            }
        }

        private void btnCustomize_Click(object sender, RoutedEventArgs e)
        {
            AppContext.GetInstance().Popup = (AppContext.GetInstance().Popup == null) ? new DraggablePopupWindow() : AppContext.GetInstance().Popup;
            AppContext.GetInstance().Popup.IsOpen = true;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lbPWSets.SelectedItem == null)
            {
                MessageBox.Show(currentLanguage.ErrMsgPWSetNotSelectedMessageBox, currentLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            MessageBoxResult result = MessageBox.Show(currentLanguage.ErrMsgConfirmDeleteMessageBox, currentLanguage.ErrConfirmCaptionMessageBox, MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                File.Delete(selectedPwSetFilePath);
                lbPWSets.Items.Remove(lbPWSets.SelectedItem);
                lbPWSets.Items.Refresh();
            }
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            if (lbPWSets.SelectedItem == null)
            {
                MessageBox.Show(currentLanguage.ErrMsgPWSetNotSelectedMessageBox, currentLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            Process.Start("notepad.exe", selectedPwSetFilePath);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(password))
            {
                MessageBox.Show(currentLanguage.ErrMsgNoPWtoSaveMessageBox, currentLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            if (lbPWSets.SelectedItem == null || string.IsNullOrEmpty(selectedPwSetFilePath))
            {
                MessageBox.Show(currentLanguage.ErrMsgPWSetNotSelectedMessageBox, currentLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            string description = Microsoft.VisualBasic.Interaction.InputBox(currentLanguage.ContentDescriptionInputBox, currentLanguage.CaptionInputBox,
                string.Empty, (int)Width / 2, (int)Height / 2);

            SavePasswordWithDescription(selectedPwSetFilePath, description);
            UIService.ShowStatus(lblStatus, UIService.BRUSH_VALID, currentLanguage.ContentPWSavedSuccesfullyStatusLabel);
            UIService.ToggleButtonImage(btnSave, ResourceLocator.PATH_ICON_SAVE_ACTIVE);
        }

        private void btnToTxt_Click(object sender, RoutedEventArgs e)
        {
            string pwSetName = Microsoft.VisualBasic.Interaction.InputBox(currentLanguage.ContentTxtFilenameInputBox, currentLanguage.CaptionInputBox,
                string.Empty, (int)Width / 2, (int)Height / 2);
            pwSetName = (!pwSetName.Contains(".txt")) ? (pwSetName + ".txt") : pwSetName;

            if (string.IsNullOrEmpty(pwSetName) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show(currentLanguage.ErrMsgInvalidInputMessageBox, currentLanguage.ErrCaptionMessageBox, MessageBoxButton.OK);
                return;
            }

            string description = Microsoft.VisualBasic.Interaction.InputBox(currentLanguage.ContentDescriptionInputBox, currentLanguage.CaptionInputBox,
                string.Empty, (int)Width / 2, (int)Height / 2);

            string pwSetsFolderPath = (ResourceLocator.appPath + ResourceLocator.PATH_OUTPUT_PW_SETS);
            SavePasswordWithDescription((pwSetsFolderPath + pwSetName), description);
            LoadPWSets();
        }

        private void SavePasswordWithDescription(string pwSetFilePath, string description)
        {
            using (var file = new StreamWriter(pwSetFilePath, true))
            {
                file.Write("pw: " + password + " => " + description);
                file.WriteLine(string.Empty);
            }
        }

        private void btnToArchive_Click(object sender, RoutedEventArgs e)
        {
            string zipName = Microsoft.VisualBasic.Interaction.InputBox(currentLanguage.ContentZipFilenameInputBox, currentLanguage.CaptionInputBox,
                string.Empty, (int)Width / 2, (int)Height / 2);
            zipName = (!zipName.Contains(".zip")) ? (zipName + ".zip") : zipName;

            if (!string.IsNullOrEmpty(zipName))
            {
                string src = (ResourceLocator.appPath + ResourceLocator.PATH_OUTPUT_PW_SETS);
                string dest = Directory.CreateDirectory(ResourceLocator.appPath + ResourceLocator.PATH_OUTPUT_ARCHIVES).FullName;

                ZipFile.CreateFromDirectory(src, dest + zipName, CompressionLevel.Fastest, true);
                Process.Start("explorer.exe", dest);
            }
        }

        private void btnToScreen_Click(object sender, RoutedEventArgs e)
        {
            string imageName = Microsoft.VisualBasic.Interaction.InputBox(currentLanguage.ContentPngFilenameInputBox, currentLanguage.CaptionInputBox,
                string.Empty, (int)Width / 2, (int)Height / 2);
            imageName = (!imageName.Contains(".png")) ? (imageName + ".png") : imageName;

            if (!string.IsNullOrEmpty(imageName))
            {
                var renderTargetBitmap = new RenderTargetBitmap((int)Width, (int)Height, 96, 96, PixelFormats.Pbgra32);
                renderTargetBitmap.Render(this);

                var pngImage = new PngBitmapEncoder();
                pngImage.Frames.Add(BitmapFrame.Create(renderTargetBitmap));

                string destination = Directory.CreateDirectory(ResourceLocator.appPath + ResourceLocator.PATH_OUTPUT_SCREENSHOTS).FullName;
                using (var fileStream = File.Create(destination + imageName))
                {
                    pngImage.Save(fileStream);
                }

                Process.Start("explorer.exe", destination);
            }
        }

        private void mainWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            currentLanguage = AppContext.GetInstance().CurrentLanguage;
            Translator.PollForTranslate(this, currentLanguage);
            AppContext.GetInstance().ResetConfiguration();
        }

        private void LoadPWSets()
        {
            lbPWSets.Items.Clear();

            string pwSetsFolderPath = ResourceLocator.appPath + ResourceLocator.PATH_OUTPUT_PW_SETS;
            allPwSetFilePaths = Directory.GetFiles(pwSetsFolderPath, "*.txt");

            foreach (var pwSetPath in allPwSetFilePaths)
            {
                AddPWSet(pwSetsFolderPath + pwSetPath);
            }
        }

        private void AddPWSet(string pwSetFullPath)
        {
            var container = new StackPanel();
            container.Orientation = Orientation.Horizontal;

            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(ResourceLocator.PATH_ICON_TEXT_ENTRY, UriKind.Relative));

            var txtIcon = new Image();
            txtIcon.Height = UIService.ICON_TXT_SIZE_HEIGHT;
            txtIcon.Width = UIService.ICON_TXT_SIZE_WIDTH;
            txtIcon.Source = brush.ImageSource;

            var tbPwSetName = new TextBlock();
            tbPwSetName.Text = Path.GetFileNameWithoutExtension(pwSetFullPath);
            tbPwSetName.Foreground = new SolidColorBrush(Colors.Black);

            container.Children.Add(txtIcon);
            container.Children.Add(tbPwSetName);

            lbPWSets.Items.Add(container);
            lbPWSets.Items.Refresh();
        }

        private void mainWindow_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            currentLanguage = AppContext.GetInstance().CurrentLanguage;
            Translator.PollForTranslate(this, currentLanguage);

            if ((AppContext.GetInstance().Popup != null) && !AppContext.GetInstance().Popup.IsOpen)
            {
                ResetVisualState();
            }

            if (AppContext.GetInstance().CustomPWLength != 0)
            {
                UIService.ShowGroupBoxDescription(grpLength, AppContext.GetInstance().CustomPWLength);
                UIService.DisableSelectors(paneRadioButtons.Children.Cast<ToggleButton>().ToArray());
            }

            if (AppContext.GetInstance().UseMeaningfulWords)
            {
                UIService.ShowGroupBoxDescription(grpSymbols, currentLanguage.HeaderAppendSymbolsGroupBox);
                UIService.DisableSelectors(chkLetters);
            }
        }

        private void ResetVisualState()
        {
            lblStatus.Content = string.Empty;
            lblStrength.Content = string.Empty;

            if (!AppContext.GetInstance().UseMeaningfulWords)
            {
                UIService.EnableSelectors(chkLetters);
            }
        }

        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoadPWSets();
        }
    }
}
