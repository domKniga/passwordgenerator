﻿using System;

namespace PasswordGenerator.Model.Exceptions
{
    [Serializable]
    public class MissingPWCatalogException : Exception
    {
        public MissingPWCatalogException(string catalog) : base("Missing catalog: " + catalog)
        {
        }

        public MissingPWCatalogException(string catalog, Exception innerException) : base("Missing catalog: " + catalog, innerException)
        {
        }
    }
}
