﻿namespace PasswordGenerator.Model.I18n
{
    public class EnglishUS : Language
    {
        public override string ContentWeakStrengthLabel { get; } = "Weak";
        public override string ContentMediumStrengthLabel { get; } = "Medium";
        public override string ContentStrongStrengthLabel { get; } = "Strong";
        public override string ContentChoicesMissingStatusLabel { get; } = "Length / symbols not selected!";
        public override string ContentLengthMissingStatusLabel { get; } = "Length not selected!";
        public override string ContentSymbolsMissingStatusLabel { get; } = "Symbols not selected!";
        public override string ContentNoPasswordToShowStatusLabel { get; } = "No password to show!";
        public override string ContentNoPasswordToCopyStatusLabel { get; } = "No password to copy!";
        public override string ErrMsgPasswordProcessingErrorMessageBox { get; } = "Password processing failed!";
        public override string ErrMsgLengthInvalidMessageBox { get; } = "Password has invalid length(should be 4-12 characters long)!";
        public override string ContentCopiedToClipboardStatusLabel { get; } = "Copied to clipboard.";
        public override string ContentStartLabel { get; } = "Start PassGen";
        public override string ContentPasswordLabel { get; } = "Password:";
        public override string TextPasswordBox { get; } = "Password appears here...";
        public override string ContentGenerateButton { get; } = "Generate";
        public override string ContentCustomizeButton { get; } = "Customize...";
        public override string HeaderLengthGroupBox { get; } = "Length";
        public override string HeaderSymbolsGroupBox { get; } = "Includes";
        public override string ContentLettersCheckBox { get; } = "Letters";
        public override string ContentNumbersCheckBox { get; } = "Numbers";
        public override string ContentSpecialsCheckBox { get; } = "Specials";
        public override string ToolTipShowButton { get; } = "Reveals the password.";
        public override string ToolTipCopyButton { get; } = "Copies the password to the clipboard.";
        public override string ToolTipGenerateButton { get; } = "Generates a password of selected length and symbol pool.";
        public override string ToolTipCustomizeButton { get; } = "Applies custom settings.";
        public override string ToolTipStartLabel { get; } = "Double-click to start.";
        public override string ContentCharsRadioButton { get; } = "chars";
        public override string ErrMsgUnknownLanguageMessageBox { get; } = "Unknown language:";
        public override string ErrCaptionMessageBox { get; } = "ERROR";
        public override string ContentPWSetsLabel { get; } = "PW sets:";
        public override string ContentSaveLabel { get; } = "Save as:";
        public override string ToolTipSaveButton { get; } = "Saves the password to the selected password set.";
        public override string ToolTipOpenButton { get; } = "Opens the selected passwor set.";
        public override string ToolTipDeleteButton { get; } = "Deletes the selected password set.";
        public override string ToolTipTxtButton { get; } = "Exports the password to a plain text file(PW Set).";
        public override string ToolTipZipButton { get; } = "Exports the PW sets to a ZIP archive.";
        public override string ToolTipPngButton { get; } = "Exports the password as a PNG screenshot.";
        public override string ContentStorageLabel { get; } = "Storage:";
        public override string ContentWordsCheckBox { get; } = "Use meaningful words.";
        public override string ContentApplyButton { get; } = "Apply";
        public override string ContentCloseButton { get; } = "Close";
        public override string ContentResetButton { get; } = "Reset";
        public override string ToolTipApplyButton { get; } = "Applies the changes.";
        public override string ToolTipCloseButton { get; } = "Closes the settings window.";
        public override string ToolTipResetButton { get; } = "Resets to default configuration.";
        public override string ToolTipLengthButton { get; } = "Sets new length for password generation.";
        public override string ToolTipLetterButton { get; } = "Adds new letter to resources.";
        public override string ToolTipNumberButton { get; } = "Adds new number to resources.";
        public override string ToolTipSpecialButton { get; } = "Adds new special symbol to resources.";
        public override string TextPWCatalogTextBlock { get; } = "PW Catalog:";
        public override string TextLanguageTextBlock { get; } = "Language:";
        public override string TextCustomSettingsTextBlock { get; } = "Custom settings";
        public override string ErrMsgPWSetNotSelectedMessageBox { get; } = "No PW set selected!";
        public override string ContentPWSavedSuccesfullyStatusLabel { get; } = "Password saved successfully.";
        public override string ErrMsgInvalidInputMessageBox { get; } = "Invalid input value!";
        public override string ErrMsgNoPWtoSaveMessageBox { get; } = "No password to save!";
        public override string ErrMsgConfirmDeleteMessageBox { get; } = "Are you sure you want to delete selected PW set?";
        public override string ErrMsgConfirmResetMessageBox { get; } = "Are you sure you want to reset to default settings?";
        public override string ErrConfirmCaptionMessageBox { get; } = "CONFIRM";
        public override string CaptionInputBox { get; } = "INPUT";
        public override string ContentDescriptionInputBox { get; } = "Specify description for the password:";
        public override string ContentTxtFilenameInputBox { get; } = "Specify filename for your PW set:";
        public override string ContentZipFilenameInputBox { get; } = "Specify name for your archive:";
        public override string ContentPngFilenameInputBox { get; } = "Specify filename for your screenshot:";
        public override string ContentNewLengthInputBox { get; } = "Specify a new length:";
        public override string ContentNewLetterInputBox { get; } = "Specify a new letter:";
        public override string ContentNewNumberInputBox { get; } = "Specify a new number:";
        public override string ContentNewSpecialInputBox { get; } = "Specify a new special symbol:";
        public override string ContentNewWordInputBox { get; } = "Specify a new word:";
        public override string ToolTipWordButton { get; } = "Adds new word to resources.";
        public override string HeaderAppendSymbolsGroupBox { get; } = "Words";
    }

}
