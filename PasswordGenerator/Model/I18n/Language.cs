﻿namespace PasswordGenerator.Model.I18n
{
    public abstract class Language : ILanguage
    {
        public enum Abbreviations { BG, EN, DE };

        public static char MaskCharacter { get; } = '*';

        public abstract string ContentWeakStrengthLabel { get; }
        public abstract string ContentMediumStrengthLabel { get; }
        public abstract string ContentStrongStrengthLabel { get; }

        public abstract string ContentChoicesMissingStatusLabel { get; }
        public abstract string ContentLengthMissingStatusLabel { get; }
        public abstract string ContentSymbolsMissingStatusLabel { get; }
        public abstract string ContentNoPasswordToShowStatusLabel { get; }
        public abstract string ContentNoPasswordToCopyStatusLabel { get; }
        public abstract string ContentCopiedToClipboardStatusLabel { get; }
        public abstract string ContentPWSavedSuccesfullyStatusLabel { get; }

        public abstract string ErrMsgInvalidInputMessageBox { get; }
        public abstract string ErrMsgLengthInvalidMessageBox { get; }
        public abstract string ErrMsgUnknownLanguageMessageBox { get; }
        public abstract string ErrMsgPasswordProcessingErrorMessageBox { get; }
        public abstract string ErrMsgPWSetNotSelectedMessageBox { get; }
        public abstract string ErrMsgNoPWtoSaveMessageBox { get; }
        public abstract string ErrMsgConfirmDeleteMessageBox { get; }
        public abstract string ErrMsgConfirmResetMessageBox { get; }
        public abstract string ErrConfirmCaptionMessageBox { get; }
        public abstract string ErrCaptionMessageBox { get; }

        public abstract string ContentStartLabel { get; }
        public abstract string ContentPasswordLabel { get; }
        public abstract string ContentSaveLabel { get; }
        public abstract string ContentPWSetsLabel { get; }
        public abstract string ContentStorageLabel { get; }

        public abstract string ContentGenerateButton { get; }
        public abstract string ContentCustomizeButton { get; }
        public abstract string ContentApplyButton { get; }
        public abstract string ContentCloseButton { get; }
        public abstract string ContentResetButton { get; }

        public abstract string HeaderLengthGroupBox { get; }
        public abstract string HeaderSymbolsGroupBox { get; }
        public abstract string HeaderAppendSymbolsGroupBox { get; }

        public abstract string ContentCharsRadioButton { get; }
        public abstract string ContentLettersCheckBox { get; }
        public abstract string ContentNumbersCheckBox { get; }
        public abstract string ContentSpecialsCheckBox { get; }
        public abstract string ContentWordsCheckBox { get; }

        public abstract string TextPasswordBox { get; }
        public abstract string TextPWCatalogTextBlock { get; }
        public abstract string TextLanguageTextBlock { get; }
        public abstract string TextCustomSettingsTextBlock { get; }

        public abstract string ToolTipShowButton { get; }
        public abstract string ToolTipCopyButton { get; }
        public abstract string ToolTipGenerateButton { get; }
        public abstract string ToolTipCustomizeButton { get; }
        public abstract string ToolTipStartLabel { get; }
        public abstract string ToolTipSaveButton { get; }
        public abstract string ToolTipOpenButton { get; }
        public abstract string ToolTipDeleteButton { get; }
        public abstract string ToolTipTxtButton { get; }
        public abstract string ToolTipZipButton { get; }
        public abstract string ToolTipPngButton { get; }
        public abstract string ToolTipApplyButton { get; }
        public abstract string ToolTipCloseButton { get; }
        public abstract string ToolTipResetButton { get; }
        public abstract string ToolTipLengthButton { get; }
        public abstract string ToolTipLetterButton { get; }
        public abstract string ToolTipNumberButton { get; }
        public abstract string ToolTipSpecialButton { get; }
        public abstract string ToolTipWordButton { get; }

        public abstract string CaptionInputBox { get; }
        public abstract string ContentDescriptionInputBox { get; }
        public abstract string ContentTxtFilenameInputBox { get; }
        public abstract string ContentZipFilenameInputBox { get; }
        public abstract string ContentPngFilenameInputBox { get; }
        public abstract string ContentNewLengthInputBox { get; }
        public abstract string ContentNewLetterInputBox { get; }
        public abstract string ContentNewNumberInputBox { get; }
        public abstract string ContentNewSpecialInputBox { get; }
        public abstract string ContentNewWordInputBox { get; }
    }

}
