﻿namespace PasswordGenerator.Model.I18n
{
    public class Bulgarian : Language
    {
        public override string ContentApplyButton { get; } = "Прилагане";
        public override string ContentCharsRadioButton { get; } = "симв.";
        public override string ContentChoicesMissingStatusLabel { get; } = "Не са избрани параметри!";
        public override string ContentCloseButton { get; } = "Затваряне";
        public override string ContentCopiedToClipboardStatusLabel { get; } = "Копирана в клипборда.";
        public override string ContentCustomizeButton { get; } = "Персонализиране...";
        public override string ContentGenerateButton { get; } = "Генериране";
        public override string ContentLengthMissingStatusLabel { get; } = "Няма избрана дължина!";
        public override string ContentLettersCheckBox { get; } = "Букви";
        public override string ContentMediumStrengthLabel { get; } = "Средна";
        public override string ContentNoPasswordToCopyStatusLabel { get; } = "Няма парола за копиране!";
        public override string ContentNoPasswordToShowStatusLabel { get; } = "Няма парола за показване!";
        public override string ContentNumbersCheckBox { get; } = "Цифри";
        public override string ContentPasswordLabel { get; } = "Парола:";
        public override string ContentPWSetsLabel { get; } = "Каталози:";
        public override string ContentResetButton { get; } = "Нулиране";
        public override string ContentSaveLabel { get; } = "Запази в:";
        public override string ContentSpecialsCheckBox { get; } = "Символи";
        public override string ContentStartLabel { get; } = "Отвори PassGen";
        public override string ContentStorageLabel { get; } = "Пакетиране:";
        public override string ContentStrongStrengthLabel { get; } = "Силна";
        public override string ContentSymbolsMissingStatusLabel { get; } = "Не са включени символи!";
        public override string ContentWordsCheckBox { get; } = "Ползване на значещи думи.";
        public override string ContentWeakStrengthLabel { get; } = "Слаба";
        public override string ErrCaptionMessageBox { get; } = "ГРЕШКА";
        public override string ErrMsgLengthInvalidMessageBox { get; } = "Невалидна дължина на парола(приемат се от 4-12 символа)!";
        public override string ErrMsgPasswordProcessingErrorMessageBox { get; } = "Грешка при обработване на паролата!";
        public override string ErrMsgUnknownLanguageMessageBox { get; } = "Невалиден език:";
        public override string HeaderLengthGroupBox { get; } = "Дължина:";
        public override string HeaderSymbolsGroupBox { get; } = "Включва:";
        public override string TextCustomSettingsTextBlock { get; } = "Настройки";
        public override string TextLanguageTextBlock { get; } = "Език:";
        public override string TextPasswordBox { get; } = "Паролата излиза тук...";
        public override string TextPWCatalogTextBlock { get; } = "Ресурси:";
        public override string ToolTipApplyButton { get; } = "Прилага направените настройки.";
        public override string ToolTipCloseButton { get; } = "Затваря прозореца за конфигурация.";
        public override string ToolTipCopyButton { get; } = "Копира паролата в клипборда.";
        public override string ToolTipCustomizeButton { get; } = "Предоставя допълнителни настойки и опции.";
        public override string ToolTipDeleteButton { get; } = "Премахва избрания каталог.";
        public override string ToolTipGenerateButton { get; } = "Генерира парола според зададените дължина и включени символи.";
        public override string ToolTipLengthButton { get; } = "Задава нова дължина за парола.";
        public override string ToolTipLetterButton { get; } = "Добавя буква по избор.";
        public override string ToolTipNumberButton { get; } = "Добавя число/цифра по избор.";
        public override string ToolTipOpenButton { get; } = "Отваря избрания каталог с пароли.";
        public override string ToolTipPngButton { get; } = "Запазва паролата като скрийншот.";
        public override string ToolTipResetButton { get; } = "Възстановява началната конфигурация.";
        public override string ToolTipSaveButton { get; } = "Запазва паролата в избрания каталог.";
        public override string ToolTipShowButton { get; } = "Разкрива паролата.";
        public override string ToolTipSpecialButton { get; } = "Добавя специален символ по избор.";
        public override string ToolTipStartLabel { get; } = "Кликни два пъти за стартиране.";
        public override string ToolTipTxtButton { get; } = "Запазва паролата, като създава нов каталог за нея.";
        public override string ToolTipZipButton { get; } = "Запазва каталозите с пароли в аржив.";
        public override string ErrMsgPWSetNotSelectedMessageBox { get; } = "Няма избран каталог с пароли!";
        public override string ContentPWSavedSuccesfullyStatusLabel { get; } = "Паролата е запазена успешно.";
        public override string ErrMsgInvalidInputMessageBox { get; } = "Невалидна предоставена стойност!";
        public override string ErrMsgNoPWtoSaveMessageBox { get; } = "Няма генерирана парола за запазване!";
        public override string ErrMsgConfirmDeleteMessageBox { get; } = "Сигурни ли сте че искате да изтриете избрания каталог с пароли?";
        public override string ErrMsgConfirmResetMessageBox { get; } = "Сигурни ли сте че искате да възстановите началните настройки?";
        public override string ErrConfirmCaptionMessageBox { get; } = "ПОТВЪРЖДЕНИЕ";
        public override string CaptionInputBox { get; } = "ДОБАВЯНЕ";
        public override string ContentDescriptionInputBox { get; } = "Добавете описание на паролата:";
        public override string ContentTxtFilenameInputBox { get; } = "Добавете име на каталога с пароли:";
        public override string ContentZipFilenameInputBox { get; } = "Добавете име на архива:";
        public override string ContentPngFilenameInputBox { get; } = "Добавете име на скрийншота:";
        public override string ContentNewLengthInputBox { get; } = "Добавете нова дължина за парола:";
        public override string ContentNewLetterInputBox { get; } = "Добавете нова буква:";
        public override string ContentNewNumberInputBox { get; } = "Добавете ново число:";
        public override string ContentNewSpecialInputBox { get; } = "Добавете нов специален символ:";
        public override string ContentNewWordInputBox { get; } = "Добавете нова дума:";
        public override string ToolTipWordButton { get; } = "Добавя нова дума към ресурсите за генерация.";
        public override string HeaderAppendSymbolsGroupBox { get; } = "Думи";
    }

}
