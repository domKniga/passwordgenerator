﻿namespace PasswordGenerator.Model.I18n
{
    public class Deutsch : Language
    {
        public override string ContentWeakStrengthLabel { get; } = "Schwach";
        public override string ContentMediumStrengthLabel { get; } = "Mittel";
        public override string ContentStrongStrengthLabel { get; } = "Stark";
        public override string ContentChoicesMissingStatusLabel { get; } = "Länge / symbole nicht ausgewählt!";
        public override string ContentLengthMissingStatusLabel { get; } = "Länge nicht ausgewählt!";
        public override string ContentSymbolsMissingStatusLabel { get; } = "Symbole nicht ausgewählt!";
        public override string ContentNoPasswordToShowStatusLabel { get; } = "Kein Passwort zu zeigen!";
        public override string ContentNoPasswordToCopyStatusLabel { get; } = "Kein Passwort zu kopieren!";
        public override string ErrMsgPasswordProcessingErrorMessageBox { get; } = "Fehler bei der Verarbeitung Passwort!";
        public override string ErrMsgLengthInvalidMessageBox { get; } = "Passwort hat ungültige Länge(sollten 4-12 Zeichen lang sein)!";
        public override string ContentCopiedToClipboardStatusLabel { get; } = "In die Zwischenablage kopiert.";
        public override string ContentStartLabel { get; } = "Offene PassGen";
        public override string ContentPasswordLabel { get; } = "Passwort:";
        public override string TextPasswordBox { get; } = "Passwort erscheint hier...";
        public override string ContentGenerateButton { get; } = "Generieren";
        public override string ContentCustomizeButton { get; } = "Anpassen...";
        public override string HeaderLengthGroupBox { get; } = "Länge";
        public override string HeaderSymbolsGroupBox { get; } = "Enthält";
        public override string ContentLettersCheckBox { get; } = "Briefen";
        public override string ContentNumbersCheckBox { get; } = "Nummern";
        public override string ContentSpecialsCheckBox { get; } = "Speziellen";
        public override string ToolTipShowButton { get; } = "Lüften Sie das Passwort.";
        public override string ToolTipCopyButton { get; } = "Kopiert das Passwort in die Zwischenablage.";
        public override string ToolTipGenerateButton { get; } = "Generieren Sie ein Passwort ausgewählter Länge und Symbole.";
        public override string ToolTipCustomizeButton { get; } = "Gilt benutzerdefinierte Einstellungen.";
        public override string ToolTipStartLabel { get; } = "Doppelklicken Sie auf Start.";
        public override string ContentCharsRadioButton { get; } = "Zeichen";
        public override string ErrMsgUnknownLanguageMessageBox { get; } = "Unbekannte Sprache:";
        public override string ErrCaptionMessageBox { get; } = "FEHLER";
        public override string ContentPWSetsLabel { get; } = "PW satz:";
        public override string ContentSaveLabel { get; } = "Sparen:";
        public override string ToolTipSaveButton { get; } = "Sparen Sie das Passwort für das ausgewählte Passwort satz.";
        public override string ToolTipOpenButton { get; } = "Öffnen Sie das gewählte Passwort satz.";
        public override string ToolTipDeleteButton { get; } = "Löschen Sie das ausgewählte Passwort satz.";
        public override string ToolTipTxtButton { get; } = "Exportieren Sie das Passwort in einer Textdatei(PW satz).";
        public override string ToolTipZipButton { get; } = "Exportieren Sie die PW Satz in ein Zip-Archiv.";
        public override string ToolTipPngButton { get; } = "Exportieren Sie das Passwort in ein PNG-Bild.";
        public override string ContentStorageLabel { get; } = "Lagerung:";
        public override string ContentWordsCheckBox { get; } = "Verwenden Sie aussagekräftige Wörter";
        public override string ContentApplyButton { get; } = "Anwenden";
        public override string ContentCloseButton { get; } = "Schließen";
        public override string ContentResetButton { get; } = "Zurückstellen";
        public override string ToolTipApplyButton { get; } = "Wenden Sie Änderungen an der Konfiguration.";
        public override string ToolTipCloseButton { get; } = "Schließen Sie das Einstellungsfenster.";
        public override string ToolTipResetButton { get; } = "Reset-Konfiguration auf die Standard.";
        public override string ToolTipLengthButton { get; } = "Setzen Sie neue Länge für Passwort-Generierung.";
        public override string ToolTipLetterButton { get; } = "Fügt neue Buchstabe Ressource.";
        public override string ToolTipNumberButton { get; } = "Fügt neue Nummer Ressource.";
        public override string ToolTipSpecialButton { get; } = "Fügt neues Symbol Ressource.";
        public override string TextPWCatalogTextBlock { get; } = "Katalog";
        public override string TextLanguageTextBlock { get; } = "Sprache";
        public override string TextCustomSettingsTextBlock { get; } = "Einstellungen";
        public override string ErrMsgPWSetNotSelectedMessageBox { get; } = "Kein PW Satz ausgewählt!";
        public override string ContentPWSavedSuccesfullyStatusLabel { get; } = "Passwort erfolgreich gespeichert.";
        public override string ErrMsgInvalidInputMessageBox { get; } = "Ungültige Eingabewert!";
        public override string ErrMsgNoPWtoSaveMessageBox { get; } = "Kein Passwort zu speichern!";
        public override string ErrMsgConfirmDeleteMessageBox { get; } = "Sind Sie sicher, dass Sie ausgewählt PW Satz zu löschen?";
        public override string ErrMsgConfirmResetMessageBox { get; } = "Sind Sie sicher, dass Sie auf die Standardeinstellungen zurücksetzen?";
        public override string ErrConfirmCaptionMessageBox { get; } = "BESTÄTIGEN";
        public override string CaptionInputBox { get; } = "EINGANG";
        public override string ContentDescriptionInputBox { get; } = "Geben Sie eine Beschreibung für das Passwort:";
        public override string ContentTxtFilenameInputBox { get; } = "Geben Sie den Dateinamen für Ihr PW Satz:";
        public override string ContentZipFilenameInputBox { get; } = "Geben Sie den Namen für Ihr Archiv:";
        public override string ContentPngFilenameInputBox { get; } = "Geben Sie den Dateinamen für den Screenshot:";
        public override string ContentNewLengthInputBox { get; } = "Geben Sie eine neue Länge:";
        public override string ContentNewLetterInputBox { get; } = "Geben Sie einen neuen Buchstabe:";
        public override string ContentNewNumberInputBox { get; } = "Geben Sie eine neue Nummer ein:";
        public override string ContentNewSpecialInputBox { get; } = "Geben Sie ein neues spezielles Symbol:";
        public override string ContentNewWordInputBox { get; } = "Geben Sie ein neues Wort:";
        public override string ToolTipWordButton { get; } = "Fügt neue Wort zu den Ressourcen.";
        public override string HeaderAppendSymbolsGroupBox { get; } = "Wörter";
    }

}
