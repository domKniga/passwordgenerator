﻿namespace PasswordGenerator.Model.I18n
{
    interface ILanguage
    {
        // Password strength messages
        string ContentWeakStrengthLabel { get; }
        string ContentMediumStrengthLabel { get; }
        string ContentStrongStrengthLabel { get; }

        // Validation messages
        string ContentChoicesMissingStatusLabel { get; }
        string ContentLengthMissingStatusLabel { get; }
        string ContentSymbolsMissingStatusLabel { get; }
        string ContentNoPasswordToShowStatusLabel { get; }
        string ContentNoPasswordToCopyStatusLabel { get; }
        string ContentCopiedToClipboardStatusLabel { get; }
        string ContentPWSavedSuccesfullyStatusLabel { get; }

        string ErrMsgInvalidInputMessageBox { get; }
        string ErrMsgLengthInvalidMessageBox { get; }
        string ErrMsgUnknownLanguageMessageBox { get; }
        string ErrMsgPasswordProcessingErrorMessageBox { get; }
        string ErrMsgPWSetNotSelectedMessageBox { get; }
        string ErrMsgNoPWtoSaveMessageBox { get; }
        string ErrMsgConfirmDeleteMessageBox { get; }
        string ErrMsgConfirmResetMessageBox { get; }
        string ErrConfirmCaptionMessageBox { get; }
        string ErrCaptionMessageBox { get; }

        // UI element contents/messages
        string ContentStartLabel { get; }
        string ContentPasswordLabel { get; }
        string ContentSaveLabel { get; }
        string ContentPWSetsLabel { get; }
        string ContentStorageLabel { get; }

        string ContentGenerateButton { get; }
        string ContentCustomizeButton { get; }
        string ContentApplyButton { get; }
        string ContentCloseButton { get; }
        string ContentResetButton { get; }

        string HeaderLengthGroupBox { get; }
        string HeaderSymbolsGroupBox { get; }
        string HeaderAppendSymbolsGroupBox { get; }

        string ContentCharsRadioButton { get; }
        string ContentLettersCheckBox { get; }
        string ContentNumbersCheckBox { get; }
        string ContentSpecialsCheckBox { get; }
        string ContentWordsCheckBox { get; }

        string TextPasswordBox { get; }
        string TextPWCatalogTextBlock { get; }
        string TextLanguageTextBlock { get; }
        string TextCustomSettingsTextBlock { get; }

        // Tooltips
        string ToolTipShowButton { get; }
        string ToolTipCopyButton { get; }
        string ToolTipGenerateButton { get; }
        string ToolTipCustomizeButton { get; }
        string ToolTipStartLabel { get; }
        string ToolTipSaveButton { get; }
        string ToolTipOpenButton { get; }
        string ToolTipDeleteButton { get; }
        string ToolTipTxtButton { get; }
        string ToolTipZipButton { get; }
        string ToolTipPngButton { get; }
        string ToolTipApplyButton { get; }
        string ToolTipCloseButton { get; }
        string ToolTipResetButton { get; }
        string ToolTipLengthButton { get; }
        string ToolTipLetterButton { get; }
        string ToolTipNumberButton { get; }
        string ToolTipSpecialButton { get; }
        string ToolTipWordButton { get; }

        string CaptionInputBox { get; }
        string ContentDescriptionInputBox { get; }
        string ContentTxtFilenameInputBox { get; }
        string ContentZipFilenameInputBox { get; }
        string ContentPngFilenameInputBox { get; }
        string ContentNewLengthInputBox { get; }
        string ContentNewLetterInputBox { get; }
        string ContentNewNumberInputBox { get; }
        string ContentNewSpecialInputBox { get; }
        string ContentNewWordInputBox { get; }
    }
}
