﻿using PasswordGenerator.Model.Exceptions;
using PasswordGenerator.Model.I18n;
using PasswordGenerator.Utilities.Locators;
using PasswordGenerator.Views;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace PasswordGenerator.Model.Configuration
{
    public sealed class AppContext
    {
        private static readonly AppContext _instance = new AppContext();
        public static AppContext GetInstance()
        {
            return _instance;
        }

        private AppContext()
        {
        }

        private DraggablePopupWindow _popup = null;
        public DraggablePopupWindow Popup
        {
            get { return _popup; }
            set { _popup = value; }
        }

        private Language _currentLanguage = ResolveCurrentLanguage();
        public Language CurrentLanguage
        {
            get { return _currentLanguage; }
            set { _currentLanguage = value; }
        }

        private bool _useMeaningfulWords = false;
        public bool UseMeaningfulWords
        {
            get { return _useMeaningfulWords; }
            set { _useMeaningfulWords = value; }
        }

        private int _customPWLength = 0;
        public int CustomPWLength
        {
            get { return _customPWLength; }
            set { _customPWLength = value; }
        }

        private ISet<int> _numbers = new HashSet<int>();
        public ISet<int> Numbers
        {
            get { return _numbers; }
            set { _numbers = value; }
        }

        private ISet<char> _letters = new HashSet<char>();
        public ISet<char> Letters
        {
            get { return _letters; }
            set { _letters = value; }
        }

        private ISet<char> _specials = new HashSet<char>();
        public ISet<char> Specials
        {
            get { return _specials; }
            set { _specials = value; }
        }

        private ISet<string> _words = new HashSet<string>();
        public ISet<string> Words
        {
            get { return _words; }
            set { _words = value; }
        }

        public void ResetConfiguration()
        {
            _popup = null;
            _useMeaningfulWords = false;
            _customPWLength = 0;
        }

        public static readonly Language DEFAULT_LANGUAGE = new EnglishUS();
        private static Language ResolveCurrentLanguage()
        {
            string confPath = (ResourceLocator.appPath + ResourceLocator.PATH_FILE_CONF);

            using (var file = new StreamReader(confPath, true))
            {
                string abbreviation = file.ReadLine();

                if (abbreviation.Equals(Language.Abbreviations.EN.ToString()))
                {
                    return new EnglishUS();
                }
                else if (abbreviation.Equals(Language.Abbreviations.DE.ToString()))
                {
                    return new Deutsch();
                }
                else if (abbreviation.Equals(Language.Abbreviations.BG.ToString()))
                {
                    return new Bulgarian();
                }
                else
                {
                    MessageBox.Show("Invalid language in configuration, defaulting to EN-US...", "ERROR", MessageBoxButton.OK);
                    return DEFAULT_LANGUAGE;
                }
            }
        }

        public void SaveCurrentLanguage()
        {
            string abbreviation = string.Empty;

            if (CurrentLanguage is Bulgarian)
            {
                abbreviation = Language.Abbreviations.BG.ToString();
            }
            else if (CurrentLanguage is Deutsch)
            {
                abbreviation = Language.Abbreviations.DE.ToString();
            }
            else
            {
                abbreviation = Language.Abbreviations.EN.ToString();
            }

            File.WriteAllText(ResourceLocator.appPath + ResourceLocator.PATH_FILE_CONF, abbreviation);
        }

        public const string LANG_EN_US = "English(EN-US)";
        public const string LANG_DE = "Deutsch(DE)";
        public const string LANG_BG = "Български(BG)";

        public ISet<string> GetAvailableLanguages()
        {
            var languages = new HashSet<string>();
            languages.Add(LANG_EN_US);
            languages.Add(LANG_DE);
            languages.Add(LANG_BG);

            return languages;
        }

        public ISet<string> GetAvailableCatalogs()
        {
            var catalogs = new HashSet<string>();
            catalogs.Add(CurrentLanguage.ContentLettersCheckBox);
            catalogs.Add(CurrentLanguage.ContentNumbersCheckBox);
            catalogs.Add(CurrentLanguage.ContentSpecialsCheckBox);
            catalogs.Add(CurrentLanguage.HeaderAppendSymbolsGroupBox);

            return catalogs;
        }

        public void LoadCatalogs()
        {
            LoadNumbers(_numbers);
            LoadLetters(_letters);
            LoadSpecials(_specials);
            LoadWords(_words);
        }

        private void LoadNumbers(ISet<int> numbers)
        {
            int number = 0;
            string numbersPath = (ResourceLocator.appPath + ResourceLocator.PATH_FILE_NUMBERS);

            using (var file = new StreamReader(numbersPath, true))
            {
                while (!file.EndOfStream)
                {
                    if (!int.TryParse(file.ReadLine(), out number))
                    {
                        throw new MissingPWCatalogException("numbers");
                    }

                    numbers.Add(number);
                }
            }
        }

        private void LoadLetters(ISet<char> letters)
        {
            char letter = '\0';
            string lettersPath = (ResourceLocator.appPath + ResourceLocator.PATH_FILE_LETTERS);

            using (var file = new StreamReader(lettersPath, true))
            {
                while (!file.EndOfStream)
                {
                    if (!char.TryParse(file.ReadLine(), out letter))
                    {
                        throw new MissingPWCatalogException("letters");
                    }

                    letters.Add(letter);
                }
            }
        }

        private void LoadSpecials(ISet<char> specials)
        {
            char special = '\0';
            string specialsPath = (ResourceLocator.appPath + ResourceLocator.PATH_FILE_SPECIALS);

            using (var file = new StreamReader(specialsPath, true))
            {
                while (!file.EndOfStream)
                {
                    if (!char.TryParse(file.ReadLine(), out special))
                    {
                        throw new MissingPWCatalogException("specials");
                    }

                    specials.Add(special);
                }
            }
        }

        private void LoadWords(ISet<string> words)
        {
            string word = string.Empty;
            string wordsPath = (ResourceLocator.appPath + ResourceLocator.PATH_FILE_WORDS);

            using (var file = new StreamReader(wordsPath, true))
            {
                while (!file.EndOfStream)
                {
                    word = file.ReadLine();
                    if (string.IsNullOrEmpty(word))
                    {
                        throw new MissingPWCatalogException("words");
                    }

                    words.Add(word);
                }
            }
        }

        //private void UpdateCatalogs(ISet<char> letterAppends, ISet<char> specialAppends, ISet<int> numberAppends, ISet<string> wordAppends)
        //{
        //    _letters.UnionWith(letterAppends);
        //    _specials.UnionWith(specialAppends);
        //    _numbers.UnionWith(numberAppends);
        //    _words.UnionWith(wordAppends);
        //}

        public void SaveCatalogAppends(ISet<char> letterAppends, ISet<char> specialAppends, ISet<int> numberAppends, ISet<string> wordAppends)
        {
            AppendCatalog((ResourceLocator.appPath + ResourceLocator.PATH_FILE_LETTERS), letterAppends.Cast<object>().ToArray());
            AppendCatalog((ResourceLocator.appPath + ResourceLocator.PATH_FILE_SPECIALS), specialAppends.Cast<object>().ToArray());
            AppendCatalog((ResourceLocator.appPath + ResourceLocator.PATH_FILE_NUMBERS), numberAppends.Cast<object>().ToArray());
            AppendCatalog((ResourceLocator.appPath + ResourceLocator.PATH_FILE_WORDS), wordAppends.Cast<object>().ToArray());

            LoadCatalogs();
        }

        private static void AppendCatalog(string filePath, object[] appends)
        {
            using (var file = new StreamWriter(filePath, true))
            {
                foreach (var append in appends)
                {
                    file.WriteLine(string.Empty);
                    file.Write(append);
                }
            }
        }
    }
}
