﻿using System;
using System.Text;
using System.Windows;

namespace PasswordGenerator
{
    public partial class App : Application
    {
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            StringBuilder errReport = new StringBuilder();
            errReport
                .Append(e.Exception.GetBaseException().Message).AppendLine().AppendLine()
                .Append("Target метод: " + e.Exception.TargetSite).AppendLine()
                .Append("Stack trace на грешката: " + e.Exception.StackTrace);

            MessageBox.Show(errReport.ToString(), "ГРЕШКА", MessageBoxButton.OK, MessageBoxImage.Error);
            Environment.Exit(1);
        }
    }
}
