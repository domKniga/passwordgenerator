﻿using System.IO;
using System.Reflection;

namespace PasswordGenerator.Utilities.Locators
{
    public static class ResourceLocator
    {
        public const string PATH_OUTPUT_ARCHIVES = @"\Resources\Output\Archives\";
        public const string PATH_OUTPUT_SCREENSHOTS = @"\Resources\Output\Screenshots\";
        public const string PATH_OUTPUT_PW_SETS = @"\Resources\Output\PWSets\";

        public const string PATH_FILE_CONF = @"\Resources\Configuration\conf.txt";
        public const string PATH_FILE_LETTERS = @"\Resources\Configuration\letters.txt";
        public const string PATH_FILE_NUMBERS = @"\Resources\Configuration\numbers.txt";
        public const string PATH_FILE_SPECIALS = @"\Resources\Configuration\specials.txt";
        public const string PATH_FILE_WORDS = @"\Resources\Configuration\words.txt";

        public const string PATH_ICON_SAVE_ACTIVE = @"\Resources\UI\saveActiveIcon.png";
        public const string PATH_ICON_SAVE_INACTIVE = @"\Resources\UI\saveIcon.png";
        public const string PATH_ICON_COPY_ACTIVE = @"\Resources\UI\copyActive.png";
        public const string PATH_ICON_COPY_INACTIVE = @"\Resources\UI\copyInactive.png";
        public const string PATH_ICON_SHOW_ACTIVE = @"\Resources\UI\showIconActive.png";
        public const string PATH_ICON_SHOW_INACTIVE = @"\Resources\UI\showIconInactive.png";
        public const string PATH_ICON_TEXT_ENTRY = @"\Resources\UI\textEntryIcon.png";

        public static readonly string appPath = ResolveAppPath();
        private static string ResolveAppPath()
        {
            string assemblyPathNode = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string binPathNode = Directory.GetParent(assemblyPathNode).FullName;

            return Directory.GetParent(binPathNode).FullName;
        }
    }
}
