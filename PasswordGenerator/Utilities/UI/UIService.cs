﻿using PasswordGenerator.Model.I18n;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PasswordGenerator.Utilities.UI
{
    public static class UIService
    {
        // Icon sizes
        public const int ICON_TXT_SIZE_HEIGHT = 20;
        public const int ICON_TXT_SIZE_WIDTH = 20;

        // Font sizes
        private const int FONT_SIZE_TITLE_OFF_FOCUS = 25;
        private const int FONT_SIZE_TITLE_ON_FOCUS = 28;

        // Highlights
        private const int THICKNESS_ERROR = 5;
        private const int THICKNESS_DEFAULT = 2;
        private static readonly Thickness HIGHLIGHT_THICKNESS_ERROR = new Thickness(THICKNESS_ERROR);
        private static readonly Thickness HIGHLIGHT_THICKNESS_DEFAULT = new Thickness(THICKNESS_DEFAULT);
        private static readonly FontWeight HIGHLIGHT_FONT = FontWeights.UltraBold;

        // Generic brushes
        public static readonly Brush BRUSH_DEFAULT = new SolidColorBrush(Colors.Black);
        public static readonly Brush BRUSH_ERROR = new SolidColorBrush(Colors.Red);
        public static readonly Brush BRUSH_WARNING = new SolidColorBrush(Colors.DarkOrange);
        public static readonly Brush BRUSH_VALID = new SolidColorBrush(Colors.Green);
        public static readonly Brush BRUSH_SELECTED = new SolidColorBrush(Colors.DeepSkyBlue);

        // Specific brushes
        private static readonly Brush BRUSH_FOCUSED_ON = new SolidColorBrush(Colors.OrangeRed);
        private static readonly Brush BRUSH_FOCUSED_OFF = new SolidColorBrush(Colors.Tan);

        public static void HidePassword(TextBox pwBox, string password)
        {
            pwBox.Text = new string(Language.MaskCharacter, password.Length);
        }

        public static void ShowPassword(TextBox pwBox, string password)
        {
            pwBox.Text = password;
        }

        public static void ShowGroupBoxDescription(GroupBox groupBox, object description)
        {
            groupBox.Foreground = BRUSH_SELECTED;
            groupBox.Header = groupBox.Header.ToString() + ":" + description.ToString();
        }

        public static void ToggleButtonImage(Button button, string imagePath)
        {
            ImageBrush brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(imagePath, UriKind.Relative));

            StackPanel container = (button.Content as StackPanel);
            Image toggleImage = container.Children.OfType<Image>().FirstOrDefault();
            toggleImage.Source = brush.ImageSource;
        }

        public static void ShowStatus(Label lblStatus, Brush statusBrush, string status)
        {
            lblStatus.Foreground = statusBrush;
            lblStatus.Content = status;
        }

        public static void SignalAlert(Label lblStatus, string alertMessage, params GroupBox[] groupBoxes)
        {
            lblStatus.Foreground = BRUSH_ERROR;
            lblStatus.FontWeight = HIGHLIGHT_FONT;
            lblStatus.Content = alertMessage;

            foreach (GroupBox groupBox in groupBoxes)
            {
                groupBox.BorderThickness = HIGHLIGHT_THICKNESS_ERROR;
                groupBox.BorderBrush = BRUSH_ERROR;
                groupBox.Foreground = BRUSH_ERROR;
            }
        }

        public static void UnsignalAlert(Label lblStatus, GroupBox groupBox, ToggleButton selector)
        {
            lblStatus.Content = string.Empty;
            selector.Foreground = BRUSH_SELECTED;

            groupBox.BorderThickness = HIGHLIGHT_THICKNESS_DEFAULT;
            groupBox.BorderBrush = BRUSH_DEFAULT;
            groupBox.Foreground = BRUSH_SELECTED;
        }

        public static void ResetGroupBoxUIState(GroupBox groupBox, ToggleButton selector, Brush stateBrush)
        {
            selector.Foreground = BRUSH_DEFAULT;
            groupBox.Foreground = stateBrush;
        }

        public static void EnableSelectors(params ToggleButton[] selectors)
        {
            foreach (ToggleButton sel in selectors)
            {
                sel.IsEnabled = true;
            }
        }

        public static void DisableSelectors(params ToggleButton[] selectors)
        {
            foreach (ToggleButton sel in selectors)
            {
                sel.IsEnabled = false;
                sel.IsChecked = false;
                sel.Foreground = BRUSH_DEFAULT;
            }
        }

        public static void HighlightLabel(Label label)
        {
            label.Foreground = BRUSH_FOCUSED_ON;
            label.FontSize = FONT_SIZE_TITLE_ON_FOCUS;
        }

        public static void UnhighlightLabel(Label label)
        {
            label.Foreground = BRUSH_FOCUSED_OFF;
            label.FontSize = FONT_SIZE_TITLE_OFF_FOCUS;
        }
    }
}
