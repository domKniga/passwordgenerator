﻿using PasswordGenerator.Model.Configuration;
using PasswordGenerator.Model.I18n;
using PasswordGenerator.Views;
using System.Windows;

namespace PasswordGenerator.Utilities.I18n
{
    public class PopupWindowTranslator : Translator
    {
        public override void Translate(FrameworkElement window, Language language)
        {
            DraggablePopupWindow popup = window as DraggablePopupWindow;

            // ComboBox translations
            popup.popCustomize.chkWords.Content = language.ContentWordsCheckBox;
            popup.popCustomize.cbCatalog.ItemsSource = AppContext.GetInstance().GetAvailableCatalogs();

            // TextBlock translations
            popup.popCustomize.tbCatalog.Text = language.TextPWCatalogTextBlock;
            popup.popCustomize.tbLanguage.Text = language.TextLanguageTextBlock;
            popup.popCustomize.tbSettings.Text = language.TextCustomSettingsTextBlock;

            // Button trnaslations
            popup.popCustomize.btnApply.Content = language.ContentApplyButton;
            popup.popCustomize.btnClose.Content = language.ContentCloseButton;
            popup.popCustomize.btnReset.Content = language.ContentResetButton;
            popup.popCustomize.btnApply.ToolTip = language.ToolTipApplyButton;
            popup.popCustomize.btnClose.ToolTip = language.ToolTipCloseButton;
            popup.popCustomize.btnReset.ToolTip = language.ToolTipResetButton;
            popup.popCustomize.btnLength.ToolTip = language.ToolTipLengthButton;
            popup.popCustomize.btnLetter.ToolTip = language.ToolTipLetterButton;
            popup.popCustomize.btnNumber.ToolTip = language.ToolTipNumberButton;
            popup.popCustomize.btnSpecial.ToolTip = language.ToolTipSpecialButton;
            popup.popCustomize.btnWord.ToolTip = language.ToolTipWordButton;
        }

    }

}
