﻿using PasswordGenerator.Model.I18n;
using System.Windows;

namespace PasswordGenerator.Utilities.I18n
{
    interface ITranslator
    {
        void Translate(FrameworkElement window, Language language);
    }
}
