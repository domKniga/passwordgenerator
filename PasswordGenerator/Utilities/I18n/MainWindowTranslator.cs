﻿using PasswordGenerator.Model.I18n;
using PasswordGenerator.Views;
using System.Windows;

namespace PasswordGenerator.Utilities.I18n
{
    public class MainWindowTranslator : Translator
    {
        public override void Translate(FrameworkElement window, Language language)
        {
            MainWindow mainWindow = window as MainWindow;

            // Label / TextBox translations
            mainWindow.lblSave.Content = language.ContentSaveLabel;
            mainWindow.lblPWSets.Content = language.ContentPWSetsLabel;
            mainWindow.lblStorage.Content = language.ContentStorageLabel;
            mainWindow.lblPassword.Content = language.ContentPasswordLabel;

            mainWindow.txtPassBox.Text = mainWindow.txtPassBox.Text.Contains("...") || string.IsNullOrEmpty(mainWindow.txtPassBox.Text)
                ? language.TextPasswordBox : mainWindow.txtPassBox.Text;

            // Button translations
            mainWindow.btnGenerate.Content = language.ContentGenerateButton;
            mainWindow.btnCustomize.Content = language.ContentCustomizeButton;
            mainWindow.btnGenerate.ToolTip = language.ToolTipGenerateButton;
            mainWindow.btnCustomize.ToolTip = language.ToolTipCustomizeButton;
            mainWindow.btnCopy.ToolTip = language.ToolTipCopyButton;
            mainWindow.btnShow.ToolTip = language.ToolTipShowButton;
            mainWindow.btnSave.ToolTip = language.ToolTipSaveButton;
            mainWindow.btnDelete.ToolTip = language.ToolTipDeleteButton;
            mainWindow.btnOpen.ToolTip = language.ToolTipOpenButton;
            mainWindow.btnToTxt.ToolTip = language.ToolTipTxtButton;
            mainWindow.btnToArchive.ToolTip = language.ToolTipZipButton;
            mainWindow.btnToScreen.ToolTip = language.ToolTipPngButton;

            // Choice element translations
            mainWindow.grpLength.Header = language.HeaderLengthGroupBox;
            mainWindow.grpSymbols.Header = language.HeaderSymbolsGroupBox;
            mainWindow.radFour.Content = "4 " + language.ContentCharsRadioButton;
            mainWindow.radEight.Content = "8 " + language.ContentCharsRadioButton;
            mainWindow.radTwelve.Content = "12 " + language.ContentCharsRadioButton;
            mainWindow.chkLetters.Content = language.ContentLettersCheckBox;
            mainWindow.chkNumbers.Content = language.ContentNumbersCheckBox;
            mainWindow.chkSpecials.Content = language.ContentSpecialsCheckBox;
        }

    }

}
