﻿using PasswordGenerator.Model.I18n;
using PasswordGenerator.Views;
using System.Windows;

namespace PasswordGenerator.Utilities.I18n
{
    public abstract class Translator : ITranslator
    {
        public abstract void Translate(FrameworkElement window, Language language);

        public static void PollForTranslate(FrameworkElement window, Language language)
        {
            if (window is StartWindow)
            {
                new StartWindowTranslator().Translate(window, language);
            }
            else if (window is MainWindow)
            {
                new MainWindowTranslator().Translate(window, language);
            }
            else if (window is DraggablePopupWindow)
            {
                new PopupWindowTranslator().Translate(window, language);
            }
        }
    }
}
