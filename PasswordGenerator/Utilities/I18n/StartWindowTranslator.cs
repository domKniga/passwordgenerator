﻿using PasswordGenerator.Model.I18n;
using PasswordGenerator.Views;
using System.Windows;

namespace PasswordGenerator.Utilities.I18n
{
    public class StartWindowTranslator : Translator
    {
        public override void Translate(FrameworkElement window, Language language)
        {
            StartWindow startWindow = window as StartWindow;
            startWindow.lblStart.Content = language.ContentStartLabel;
            startWindow.lblStart.ToolTip = language.ToolTipStartLabel;
        }

    }

}
