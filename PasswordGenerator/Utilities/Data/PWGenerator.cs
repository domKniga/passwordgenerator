﻿using PasswordGenerator.Model.Configuration;
using PasswordGenerator.Model.I18n;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace PasswordGenerator.Utilities.Data
{
    enum ResourceType : int { Letter = 1, Number = 2, Special = 3, Word = 4 };

    public static class PWGenerator
    {
        private static readonly Random rng = new Random();
        private static bool capitalizeWord = false;

        public static string GeneratePassword(int length, bool lettersOn, bool numbersOn, bool specialsOn, bool wordsOn)
        {
            StringBuilder pwBuilder = new StringBuilder();

            List<int> pwResourcePool = GetResourcePool(lettersOn, numbersOn, specialsOn, wordsOn);

            int randMinimum = pwResourcePool.ElementAt(0);
            int randMaximum = pwResourcePool.ElementAt(pwResourcePool.Count - 1);

            while (true)
            {
                int randomResourceType = rng.Next(randMinimum, randMaximum + 1);

                if (!pwResourcePool.Contains(randomResourceType))
                {
                    continue;
                }

                switch (randomResourceType)
                {
                    case (int)ResourceType.Letter:
                        pwBuilder.Append(GetRandomResource(AppContext.GetInstance().Letters));
                        break;
                    case (int)ResourceType.Number:
                        pwBuilder.Append(GetRandomResource(AppContext.GetInstance().Numbers));
                        break;
                    case (int)ResourceType.Special:
                        pwBuilder.Append(GetRandomResource(AppContext.GetInstance().Specials));
                        break;
                    case (int)ResourceType.Word:
                        pwBuilder.Append(ToggleCapitalization(GetRandomResource(AppContext.GetInstance().Words)));
                        break;
                    default:
                        Language language = AppContext.GetInstance().CurrentLanguage;
                        MessageBox.Show(language.ErrMsgPasswordProcessingErrorMessageBox, language.ErrCaptionMessageBox, MessageBoxButton.OK);
                        break;
                }

                if (pwBuilder.Length > length)
                {
                    pwBuilder.Remove(0, pwBuilder.Length);
                }

                string generatedPassword = pwBuilder.ToString();
                if ((generatedPassword.Length == length) && PWValidator.ValidateGeneratedPassword(generatedPassword, lettersOn, numbersOn, specialsOn, wordsOn))
                {
                    return generatedPassword;
                }
            }
        }

        private static List<int> GetResourcePool(bool lettersOn, bool numbersOn, bool specialsOn, bool wordsOn)
        {
            List<int> resourcePool = new List<int>();

            if (lettersOn)
            {
                resourcePool.Add((int)ResourceType.Letter);
            }

            if (numbersOn)
            {
                resourcePool.Add((int)ResourceType.Number);
            }

            if (specialsOn)
            {
                resourcePool.Add((int)ResourceType.Special);
            }

            if (wordsOn)
            {
                resourcePool.Add((int)ResourceType.Word);
            }

            return resourcePool;
        }

        private static T GetRandomResource<T>(ISet<T> resourceSet)
        {
            int resourceCount = resourceSet.Count;
            int randomResourceIdx = rng.Next(0, resourceCount);

            return resourceSet.ElementAt(randomResourceIdx);
        }

        private static string ToggleCapitalization(string word)
        {
            capitalizeWord = !capitalizeWord;

            char[] wordLetters = word.ToCharArray();
            wordLetters[0] = capitalizeWord ? char.ToUpper(wordLetters[0]) : char.ToLower(wordLetters[0]);

            return new string(wordLetters);
        }

    }

}
