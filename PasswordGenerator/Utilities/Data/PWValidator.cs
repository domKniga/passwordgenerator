﻿using PasswordGenerator.Model.Configuration;
using PasswordGenerator.Model.I18n;
using PasswordGenerator.Utilities.UI;
using System.Windows;
using System.Windows.Controls;

namespace PasswordGenerator.Utilities.Data
{
    enum Length : int { Minimum = 4, Average = 8, Maximum = 12, Weak = 6, Medium = 10 };

    public static class PWValidator
    {
        public static bool IsLengthSelected(UIElementCollection radioButtons)
        {
            bool isRadioButtonSelected = false;

            foreach (RadioButton radioButton in radioButtons)
            {
                isRadioButtonSelected = radioButton.IsChecked ?? false;

                if (isRadioButtonSelected || AppContext.GetInstance().CustomPWLength != 0)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool AreSymbolsIncluded(UIElementCollection checkBoxes)
        {
            if (AppContext.GetInstance().UseMeaningfulWords)
            {
                return true;
            }

            bool isCheckBoxSelected = false;

            foreach (CheckBox checkBox in checkBoxes)
            {
                isCheckBoxSelected = checkBox.IsChecked ?? false;

                if (isCheckBoxSelected)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool ValidatePasswordLength(string password)
        {
            Language language = AppContext.GetInstance().CurrentLanguage;

            int passwordLength = password.Length;

            if (AppContext.GetInstance().CustomPWLength != 0)
            {
                return true;
            }

            if ((passwordLength < (int)Length.Minimum) || (passwordLength > (int)Length.Maximum))
            {
                MessageBox.Show(language.ErrMsgLengthInvalidMessageBox, language.ErrCaptionMessageBox, MessageBoxButton.OK);
                return false;
            }

            return true;
        }

        public static bool ValidateGeneratedPassword(string password, bool lettersOn, bool numbersOn, bool specialsOn, bool wordsOn)
        {
            bool hasNoLowercase = (!containsLowercase(password));
            bool hasNoUppercase = (!containsUppercase(password));
            bool hasNoNumber = (!containsNumber(password));
            bool hasNoSpecial = (!containsSpecial(password));

            if (wordsOn && (hasNoLowercase && hasNoUppercase))
            {
                return false;
            }

            if (lettersOn && (hasNoLowercase && hasNoUppercase))
            {
                return false;
            }

            if (numbersOn && hasNoNumber)
            {
                return false;
            }

            if (specialsOn && hasNoSpecial)
            {
                return false;
            }

            return true;
        }

        public static void DisplayPasswordStrength(Label lblStrength, string password)
        {
            Language language = AppContext.GetInstance().CurrentLanguage;

            int passwordLength = password.Length;

            bool hasWeakLength = (passwordLength < (int)Length.Weak);
            bool hasMediumLength = (passwordLength < (int)Length.Medium);
            bool hasNoLowercase = (!containsLowercase(password));
            bool hasNoUppercase = (!containsUppercase(password));
            bool hasNoNumber = (!containsNumber(password));
            bool hasNoSpecial = (!containsSpecial(password));

            if (hasWeakLength || hasNoLowercase || hasNoUppercase)
            {
                lblStrength.Foreground = UIService.BRUSH_ERROR;
                lblStrength.Content = language.ContentWeakStrengthLabel;
            }
            else if (hasMediumLength || hasNoNumber || hasNoSpecial)
            {
                lblStrength.Foreground = UIService.BRUSH_WARNING;
                lblStrength.Content = language.ContentMediumStrengthLabel;
            }
            else
            {
                lblStrength.Foreground = UIService.BRUSH_VALID;
                lblStrength.Content = language.ContentStrongStrengthLabel;
            }
        }

        private static bool containsUppercase(string password)
        {
            foreach (char c in password)
            {
                if (char.IsUpper(c))
                {
                    return true;
                }

            }
            return false;
        }

        private static bool containsLowercase(string password)
        {
            foreach (char c in password)
            {
                if (char.IsLower(c))
                {
                    return true;
                }

            }
            return false;
        }

        private static bool containsNumber(string password)
        {
            foreach (char c in password)
            {
                if (char.IsDigit(c))
                {
                    return true;
                }

            }
            return false;
        }

        private static bool containsSpecial(string password)
        {
            foreach (char c in password)
            {
                if (!char.IsLetterOrDigit(c))
                {
                    return true;
                }

            }
            return false;
        }

    }

}
